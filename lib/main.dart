import 'package:flutter/material.dart';
import 'package:individualtasks/transport_task/UI/main_page.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Arrow Path Example',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: MainPage(),
    );
  }
}