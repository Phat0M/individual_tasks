import 'dart:async';

import 'package:flutter/material.dart';
import 'package:individualtasks/transport_task/logic/entities.dart';

class DataSelector extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => DataSelectorState();

}

class DataSelectorState extends State<DataSelector> {

  Data currentData;

  List<FocusNode> focusNodes;

  static const textStyle = TextStyle(fontSize: 20);

  static const spacer = SizedBox(height: 5,);

  StreamController<Data> controller = StreamController.broadcast();

  Stream<Data> get stream => controller.stream;

  GlobalKey<FormState> storageA;
  GlobalKey<FormState> storageB;
  bool clearA = true;
  bool clearB = true;

  void sink(Data data) {
    controller.sink.add(data);
  }

  @override
  void dispose() {
    super.dispose();
    controller.close();
  }

  @override
  void initState() {
    super.initState();
    currentData = Data();
    focusNodes = List.generate(11, (index) => FocusNode());
    stream.listen((event) {
      currentData = event;
    });
    storageA = GlobalKey<FormState>();
    storageB = GlobalKey<FormState>();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            width: 300,
            child: formFieldBuild(
                index: 0,
                validator: numberValidator,
                func: (String text) {
                  if(numberValidator(text) == null) {
                    var data = currentData ?? Data();
                    sink(data.copyWith(distance: double.parse(text)));
                  }
                },
                labelText: "Расстояние между фирмами, км."
            ),
          ),
          SizedBox(height: 10,),
          Container(padding: EdgeInsets.symmetric(vertical: 10),
            child: Row(
              children: <Widget>[
                Expanded(
                    child: Center(
                        child: Text('A', style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),)
                    )
                ),
                Expanded(
                  child: Center(
                      child: Text('B', style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),)
                  ),
                )
              ],
            ),
          ),
          spacer,
          headerBuild('Производственные затраты'),
          spacer,
          Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Row(
                children: <Widget>[
                  Expanded(
                      child: formFieldBuild(
                        index: 1,
                        labelText: 'Затраты А',
                        validator: numberValidator,
                        func: (String text) {
                          if(numberValidator(text) == null) {
                            var data = currentData ?? Data();
                            var company = data.first ?? Company();
                            sink(data.copyWith(first: company.copyWith(cost: double.parse(text))));
                          }
                        },
                      )
                  ) ,
                  SizedBox(width: 10,),
                  Expanded(
                      child: formFieldBuild(
                        index: 2,
                        labelText: 'Затраты B',
                        validator: numberValidator,
                        func: (String text) {
                          if(numberValidator(text) == null) {
                            var data = currentData ?? Data();
                            var company = data.second ?? Company();
                            sink(data.copyWith(second: company.copyWith(cost: double.parse(text))));
                          }
                        },
                      )
                  ) ,
                ]
            ),
          ),
          spacer,
          headerBuild('Транспортный тариф, у.д.е./км'),
          spacer,
          Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Row(
                children: <Widget>[
                  Expanded(
                      child: formFieldBuild(
                        index: 3,
                        labelText: 'Тариф А',
                        validator: numberValidator,
                        func: (String text) {
                          if(numberValidator(text) == null) {
                            var data = currentData ?? Data();
                            var company = data.first ?? Company();
                            sink(data.copyWith(first: company.copyWith(tariff: double.parse(text))));
                          }
                        },
                      )
                  ) ,
                  SizedBox(width: 10,),
                  Expanded(
                      child: formFieldBuild(
                        index: 4,
                        labelText: 'Тариф B',
                        validator: numberValidator,
                        func: (String text) {
                          if(numberValidator(text) == null) {
                            var data = currentData ?? Data();
                            var company = data.second ?? Company();
                            sink(data.copyWith(second: company.copyWith(tariff: double.parse(text))));
                          }
                        },
                      )
                  ) ,
                ]
            ),
          ),
          spacer,
          headerBuild('Склады'),
          spacer,
          headerBuild('Чтобы добавить склад, заполните\nвсе соответствующие поля'),
          spacer,
          headerBuild('Расстояния до фирм'),
          spacer,
          Row(
            children: <Widget>[
              Expanded(
                child: Form(
                  child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                                child: formFieldBuild(
                                  index: 5,
                                  labelText: 'До А',
                                  validator: (data) => storageValidator(data, storageA, clearA),
                                  func: (String text) {
                                    if(numberValidator(text) == null) {
                                      var data = currentData ?? Data();
                                      var company = data.first ?? Company();
                                      var storage = company.storage ?? Storage();
                                      sink(data.copyWith(
                                          first: company.copyWith(
                                              storage: storage.copyWith(
                                                  distanceToA: double.parse(text)
                                              ))));
                                    }
                                  },
                                )
                            ) ,
                            Expanded(
                                child: formFieldBuild(
                                  index: 6,
                                  labelText: 'До B',
                                  validator: (data) => storageValidator(data, storageA, clearA),
                                  func: (String text) {
                                    if(numberValidator(text) == null) {
                                      var data = currentData ?? Data();
                                      var company = data.first ?? Company();
                                      var storage = company.storage ?? Storage();
                                      sink(data.copyWith(
                                          first: company.copyWith(
                                              storage: storage.copyWith(
                                                  distanceToB: double.parse(text)
                                              ))));

                                    }
                                  },
                                )
                            ) ,
                          ],
                        ),
                        spacer,
                        formFieldBuild(
                          index: 7,
                          labelText: 'Тариф A',
                          validator: (data) => storageValidator(data, storageA, clearA),
                          func: (String text) {
                            if(numberValidator(text) == null) {
                              var data = currentData ?? Data();
                              var company = data.first ?? Company();
                              var storage = company.storage ?? Storage();
                              sink(data.copyWith(
                                  first: company.copyWith(
                                      storage: storage.copyWith(
                                          cost: double.parse(text)
                                      ))));
                            }
                          },
                        )
                      ]
                  ),
                ),
              ),
              SizedBox(width: 10,),
              Expanded(
                child: Form(
                  child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                                child: formFieldBuild(
                                  index: 8,
                                  labelText: 'До А',
                                  validator: (data) => storageValidator(data, storageB, clearB),
                                  func: (String text) {
                                    if(numberValidator(text) == null) {
                                      var data = currentData ?? Data();
                                      var company = data.second ?? Company();
                                      var storage = company.storage ?? Storage();
                                      sink(data.copyWith(
                                          second: company.copyWith(
                                              storage: storage.copyWith(
                                                  distanceToA: double.parse(text)
                                              ))));
                                    }
                                  },
                                )
                            ) ,
                            Expanded(
                                child: formFieldBuild(
                                  index: 9,
                                  labelText: 'До B',
                                  validator: (data) => storageValidator(data, storageB, clearB),
                                  func: (String text) {
                                    if(numberValidator(text) == null) {
                                      var data = currentData ?? Data();
                                      var company = data.second ?? Company();
                                      var storage = company.storage ?? Storage();
                                      sink(data.copyWith(
                                          second: company.copyWith(
                                              storage: storage.copyWith(
                                                  distanceToB: double.parse(text)
                                              ))));
                                    }
                                  },
                                )
                            ) ,
                          ],
                        ),
                        spacer,
                        formFieldBuild(
                          index: 10,
                          labelText: 'Тариф B',
                          validator: (data) => storageValidator(data, storageB, clearB),
                          func: (String text) {
                            if(numberValidator(text) == null) {
                              var data = currentData ?? Data();
                              var company = data.second ?? Company();
                              var storage = company.storage ?? Storage();
                              sink(data.copyWith(
                                  second: company.copyWith(
                                      storage: storage.copyWith(
                                          cost: double.parse(text)
                                      ))));
                            }
                          },
                        )
                      ]
                  ),
                ),
              ),
            ],
          ),
          spacer,
//          Container(
//            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
//            child: RaisedButton(
//              color: Colors.green,
//              onPressed: () {
//
//              },
//              child: Text("Расчитать",
//                  textAlign: TextAlign.center,
//                  style: TextStyle(color: Colors.white, fontSize: 20)),
//            ),
//          ),
          spacer,
          Divider(height: 5, color: Colors.black,),
          spacer,
          sheetBuild(context),
        ],
      ),
    );
  }

  String storageValidator(String text,GlobalKey<FormState> states, bool clear) {
    String error;
    if(text.length == 0) {
      error = null;
    }
    else if(double.tryParse(text) == null) {
      error = "Введите число";
    }
    else if (double.parse(text) <= 0) {
      error = "Должно быть больше 0";
    }
    return error;
  }

  Text headerBuild(String head) =>
      Text(head, textAlign: TextAlign.center, style: textStyle,);

  String numberValidator(String text) {
    if(text.length == 0) {
      return "Обязательное поле";
    }
    if(double.tryParse(text) == null) {
      return "Введите число";
    }
    else if (double.parse(text) <= 0) {
      return "Должно быть больше 0";
    }
    return null;
  }

  TextFormField formFieldBuild({
    GlobalKey<FormState> key,
    int index,
    OnSubmited func,
    FormFieldValidator<String> validator,
    String labelText
  }) {
    return TextFormField(
      key: key,
      focusNode: focusNodes[index],
      autovalidate: true,
      keyboardType: TextInputType.number,
      validator: validator,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
        labelText: labelText,
        border: OutlineInputBorder(
          borderSide: BorderSide(),
        ),
      ),
      onFieldSubmitted: (String data) {
        if(validator(data) == null) {
          if(index + 1 < focusNodes.length) {
            focusNodes[index + 1].requestFocus();
          }
          func(data);
        }
      },
    );
  }

  Widget sheetBuild(BuildContext context) {
    return StreamBuilder<Data> (
      stream: stream,
      initialData: null,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if(snapshot.hasData) {
          return dataSheetBuild(snapshot.data, context);
        }
        else {
          return Container(
            child: Text('Введите данны, помеченные\nкак "Обязательное поле"'),
          );
        }
      },
    );
  }

  Widget dataSheetBuild(Data data, BuildContext context) {
    return Container(
      child: Text(data.toString()),
    );
  }

}

typedef OnSubmited = void Function(String data);