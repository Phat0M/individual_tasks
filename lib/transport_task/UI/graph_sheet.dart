//import 'dart:math';
//
//import 'package:arrow_path/arrow_path.dart';
//import 'package:flutter/material.dart';
//import 'package:individualtasks/transport_task/logic/entities.dart';
//
//
//class GraphSheet extends StatefulWidget {
//
//  final List<Edge> edges;
//  final List<Vertex> vertices;
//
//  GraphSheet({
//    Key key,
//    this.edges,
//    this.vertices
//  }) : assert(edges != null && vertices != null),
//        assert(edges.length > 0 && vertices.length > 1),
//        assert(edges.length <= vertices.length * (vertices.length - 1)),
//        super(key: key);
//
//
//
//  @override
//  State<StatefulWidget> createState() => _GraphSheetState();
//}
//
//const radius = 25.0;
//
//class _GraphSheetState extends State<GraphSheet> {
//
//  List<Offset> offsets;
//
//  @override
//  void initState() {
//    super.initState();
//    offsets = List.generate(widget.vertices.length, (index) {
//      return Offset(index * radius, index * radius);
//    });
//
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    int gp = 0;
//    widget.edges.forEach((i) {
//      gp += i.optimality + i.supply.abs();
//    });
//    List<Widget> widgets = List.generate(offsets.length, (index) {
//      return buildVertex(index);
//    });
//    widget.edges.forEach((edge) {
//      widgets.add(buildEdge(edge.first.number, edge.second.number, edge));
//    });
//    return Scaffold(
//      body: Column(
//        children: <Widget>[
//          Container(
//            height: 400,
//              width: 500,
//              child: Stack(
//            children: widgets,
//          )),
//          Text('Затраты $gp')
//        ],
//      ),
//    );
//  }
//
//  Widget buildVertex(int index) {
//    return AnimatedPositioned(
//      duration: Duration(microseconds: 1),
//      left: offsets[index].dx,
//      top: offsets[index].dy,
//      height: radius * 2,
//      width: radius * 2,
//      child: GestureDetector(
//        onPanUpdate: (details) {
//          var dx = offsets[index].dx + details.delta.dx;
//          var dy  = offsets[index].dy +  details.delta.dy;
//          offsets[index] = Offset(dx, dy);
//          setState(() {});
//        },
//        child: _VertexWidget(
//          size: radius * 2,
//          vertex: widget.vertices[index],
//        ),
//      ),
//    );
//  }
//
//  Widget buildEdge(int from, int to, Edge edge) {
//    var f = offsets[from];
//    var t = offsets[to];
//    return _EdgeWidget(edge: edge, first: Offset(f.dx + radius, f.dy + radius), second: Offset(t.dx + radius, t.dy + radius),);
//  }
//
//}
//
//class _VertexWidget extends StatelessWidget {
//
//  final double size;
//  final Vertex vertex;
//
//  _VertexWidget({
//    this.size = 50,
//    this.vertex,
//  });
//
//
//
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      height: size,
//      width: size,
//      decoration: BoxDecoration(
//        shape: BoxShape.circle,
//        color: Colors.white30,
//        border: Border.all(color: Colors.black, width: 2.0),
//      ),
//      child: Column(
//        children: <Widget>[
//          Expanded(
//            child: Center(
//              child: Text(vertex.production.toString()),
//            ),
//          ),
//          Divider(
//            height: 2,
//            color: Colors.black,
//          ),
//          Expanded(
//            child: Center(
//              child: Text((vertex.satisfaction ?? 0).toString()),
//            ),
//          )
//        ],
//      ),
//
//    );
//  }
//}
//
//class _EdgeWidget extends StatelessWidget {
//
//  final Edge edge;
//  final Offset first;
//  final Offset second;
//
//  _EdgeWidget({
//    this.edge,
//    this.first,
//    this.second,
//  });
//
//  @override
//  Widget build(BuildContext context) {
//    return CustomPaint(
//      painter: _ArrowPainter(first: first, second: second, edge: edge),
//    );
//  }
//}
//
//class _ArrowPainter extends CustomPainter {
//
//  final Offset first;
//  final Offset second;
//  final Edge edge;
//
//  _ArrowPainter({
//    this.first,
//    this.second,
//    this.edge,
//  });
//
//  @override
//  void paint(Canvas canvas, Size size) {
//
//    /// first is height ?
//    bool horizontal = this.first.dx < this.second.dx;
//    /// first is left ?
//    bool vertical = this.first.dy < this.second.dy;
//
//    double distanceX = (this.first.dx - this.second.dx).abs();
//    double distanceY = (this.first.dy - this.second.dy).abs();
//
//    double distance = sqrt(pow(distanceX, 2) + pow(distanceY, 2));
//
//    double y = radius * sin(asin(distanceY / distance));
//    double x = radius * sin(asin(distanceX / distance));
//
//    //coordinate of first circle
//    Offset first = Offset(horizontal ? this.first.dx + x : this.first.dx - x, vertical ? this.first.dy + y : this.first.dy - y);
//    //coordinate of second circle
//    Offset second = Offset(horizontal ? this.second.dx - x : this.second.dx + x, vertical ? this.second.dy - y : this.second.dy + y);
//
//    Paint paint = Paint()
//      ..color = Colors.blue
//      ..strokeWidth = 2.0
//      ..style = PaintingStyle.stroke;
//    Path path = Path();
//    if(edge.supply >= 0) {
//      path.moveTo(first.dx, first.dy);
//      path.lineTo(second.dx, second.dy);
//    }
//    else if (edge.supply < 0) {
//      path.moveTo(second.dx, second.dy);
//      path.lineTo(first.dx, first.dy);
//    }
//    if(edge.supply != 0) {
//      path = ArrowPath.make(path: path);
//    }
//
//    var textSpan = TextSpan(
//      text: '${edge.optimality.abs()} : ${edge.supply.abs()}',
//      style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: radius * 2 / 3),
//    );
//
//    var textPainter = TextPainter(
//      text: textSpan,
//      textAlign: TextAlign.center,
//      textDirection: TextDirection.ltr,
//    );
//    textPainter.layout(minWidth: size.width);
//
//    var radiusX = distanceX / 2;
//    var radiusY = distanceY / 2;
//
//    var offset = Offset(
//        (first.dx + second.dx) / 2 - 10,
//        (first.dy + second.dy) / 2 - 10
//    );
//    canvas.drawPath(path, paint);
//    textPainter.paint(canvas, offset);
//  }
//
//  @override
//  bool shouldRepaint(CustomPainter oldDelegate) => true;
//
//}