import 'package:flutter/material.dart';
import 'package:individualtasks/transport_task/UI/data_selector.dart';

class MainPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _MainPageState();


}

class _MainPageState extends State<MainPage> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Определение границ рынка сбыта"),),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  DataSelector(),
                ],
              ),
            ),
          ),
        )
    );
  }

}