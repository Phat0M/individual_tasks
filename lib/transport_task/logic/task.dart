import 'package:individualtasks/transport_task/logic/entities.dart';

class DataTask {

  final Data data;
  double koef = 0.0;
  double Xa = 0.0;
  double Xb = 0.0;

  DataTask(this.data);

  bool calculate() {
    var first = data.first;
    var second = data.second;
    var distance = data.distance;
    if (first != null && second != null && distance != null
        && first.cost != null && first.tariff != null
        && second.cost != null && second.tariff != null) {
      Xa = ((second.cost - first.cost) / second.tariff)
          * (1 / (first.tariff / second.tariff - distance));
      Xb = distance - Xa;
      this.koef = Xa / distance;
      return true;
    }
    else return false;
  }
}