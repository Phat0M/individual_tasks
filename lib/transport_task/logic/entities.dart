class Data {
  final Company first;
  final Company second;
  final double distance;

  Data({
    this.first,
    this.second,
    this.distance
  });

  Data copyWith({
    Company first,
    Company second,
    double distance
  }) {
    return Data(
        first: first ?? this.first,
        second: second ?? this.second,
        distance: distance ?? this.distance
    );
  }

  @override
  String toString() {
    return 'Data{first: ${first.toString()}, second: ${second.toString()}, distance: $distance}';
  }


}

class Company {
  final Storage storage;
  final double cost;
  final double tariff;

  Company({
    this.storage,
    this.cost,
    this.tariff
  });

  Company copyWith({
    Storage storage,
    double cost,
    double tariff
  }) {
    return Company(
        storage: storage ?? this.storage,
        cost: cost ?? this.cost,
        tariff: tariff ?? this.tariff
    );
  }

  @override
  String toString() {
    return 'Company{storages: ${storage.toString()}, cost: $cost, tariff: $tariff}';
  }
}

class Storage {
  final double distanceToA;
  final double distanceToB;
  final double cost;

  Storage({
    this.distanceToA,
    this.distanceToB,
    this.cost
  });

  Storage copyWith({
    double distanceToA,
    double distanceToB,
    double cost
  }) {
    return  Storage(
      distanceToA: distanceToA ?? this.distanceToA,
      distanceToB: distanceToB ?? this.distanceToB,
      cost: cost ?? this.cost,
    );
  }

  @override
  String toString() {
    return 'Storage{distanceToA: $distanceToA, distanceToB: $distanceToB, cost: $cost}';
  }
}